from flask import Flask, render_template

from features.weather import weather_features
path_to_page = 'weather_page/weather_page.htm'

def show_page(app):
	brest_temp, brest_cond = weather_features.get_current_temperature_by_city('Brest')
	minsk_temp, minsk_cond = weather_features.get_current_temperature_by_city('Minsk')
	moscow_temp, moscow_cond = weather_features.get_current_temperature_by_city('Moscow')

	return render_template(path_to_page,
		 brest_temp = brest_temp,
		 minsk_temp = minsk_temp,
		 moscow_temp = moscow_temp, 
		 minsk_cond = minsk_cond,
		 moscow_cond = moscow_cond,
		 brest_cond = brest_cond)