from flask import Flask, render_template

path_to_page = 'login_page/login_page.htm'

def show_page(app):
 	return render_template(path_to_page)