from flask import Flask, send_from_directory

app = Flask(__name__,
	template_folder="./html/templates/",
	static_folder='html/static',
	static_url_path='')

from route.login_page import login_page
from route.weather_page import weather_page

@app.route('/')
def login():
    return login_page.show_page(app)

@app.route('/weather')
def weather():
	return weather_page.show_page(app)

if __name__ == '__main__':
    app.run()