import requests

appid = "5c45357663f367abd6a3bfa72b2f453f"

def get_current_temperature_by_city(s_city):
    city_id = 0

    try:
        res = requests.get("http://api.openweathermap.org/data/2.5/find",
                 params={'q': s_city, 'type': 'like', 'units': 'metric', 'APPID': appid})
        data = res.json()
        cities = ["{} ({})".format(d['name'], d['sys']['country'])
              for d in data['list']]
        city_id = data['list'][0]['id']
    except Exception as e:
        print("Exception (find):", e)
        pass

    try:
        res = requests.get("http://api.openweathermap.org/data/2.5/weather",
                 params={'id': city_id, 'units': 'metric', 'lang': 'ru', 'APPID': appid})
        data = res.json()

        return data['main']['temp'], data['weather'][0]['description']
    except Exception as e:
        print("Exception (weather):", e)
    pass
